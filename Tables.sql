 /* _        _     _ */
/* | |_ __ _| |__ | | ___  ___ */
/* | __/ _` | '_ \| |/ _ \/ __| */
/* | || (_| | |_) | |  __/\__ \ */
 /* \__\__,_|_.__/|_|\___||___/ */

 drop table email_table;

/*^*/
create extension pgcrypto;
select * from pg_proc where proname like 'gen_random_%';

create table email_table (
	/* emailId UUID primary key default gen_random_uuid(), */
	emailid serial primary key not null unique,
	email text not null unique
);

create table password_table (
    /* emailId UUID references email_table_ on delete cascade not null unique, */
	/* passwordid UUID primary key default gen_random_uuid(), */
	passwordid serial primary key not null unique,
    emailid integer references email_table on delete cascade not null unique,
    password text not null
);

create table identity_table (
	/* identityid UUID primary key default gen_random_uuid(), */
    /* emailid integer references email_table_ on delete cascade not null unique, */
	identityid serial primary key not null unique,
    emailid integer references email_table on delete cascade not null unique,
    lastname text not null,
    firstname text not null,
    birthdate date not null,
    address text not null,
    phone text not null
);

create table admin_table (
	/* adminid UUID primary key default gen_random_uuid(), */
    /* emailid UUID references email_table_ on delete cascade not null unique, */
	adminid serial primary key not null unique,
    emailid integer references email_table on delete cascade not null unique
);
/*$*/

      /* _ */
  /* ___| |_ ___ */
 /* / _ \ __/ __| */
/* |  __/ || (__ */
 /* \___|\__\___| */

select * from email_table_test_;
select * from identity_table_test_;
select * from password_table_test_;


select * from email_table_ e inner join identity_table_ i on e.emailid = i.emailid;
select * from email_table_;
select * from identity_table_;
select * from password_table_;


truncate table email_table_test_;
truncate table identity_table_test_;
truncate table password_table_test_;

/*
vim: fdm=marker fmr=*^*,*$*
 */
