#!/bin/sh

# source ./env.sh
# [ -z $team2url ] && exit 1
# [ -z $team2user ] && exit 1
# [ -z $team2pass ] && exit 1

# f00=./src/main/java/dev/team2/daos/sql2o/SubscriberDao.java
# tmp00=$(mktemp /tmp/team2webappXXX)
# cp $f00 $tmp00
# echo $team2url
# sed -e "/\/\ url/ s!.*!\"${team2url}\",!" \
#         -e "/USERNAME/ s/.*/\"${team2user}\",/" \
#         -e "/PASSWORD/ s/.*/\"${team2pass}\"/" \
#         -i $f00
        # $f00 > /tmp/test00
        # -e "/url/ s!<++>!$team2url!" \
        # -e "/USERNAME/ s/<++>/$team2user/" \
        # -e "/PASSWORD/ s/<++>/$team2pass/" \
# fi

# f=./src/main/java/dev/team2/util/TempConnectionUtil.java
# tmp=$(mktemp /tmp/team2webappXXX)
# if cp $f $tmp; then
#     sed -e '/final String TARGET/ s/System\.getenv("TARGET")/"tomcat9"/' \
#         -e "/url/ s!<++>!$team2url!" \
#         -e "/USERNAME/ s/<++>/$team2user/" \
#         -e "/PASSWORD/ s/<++>/$team2pass/" \
#         -i $f
# fi

if mvn package; then

    sudo systemctl stop tomcat9.service

    sudo rm -rf /var/lib/tomcat9/webapps/*

    # access via http://localhost:8080/foo-servlet
    sudo cp ./target/team-2-webapp-1.0-SNAPSHOT.war \
        /var/lib/tomcat9/webapps/ROOT.war
    sudo chown tomcat9:tomcat9 /var/lib/tomcat9/webapps/ROOT.war

    sudo systemctl start tomcat9.service

fi

# cp $tmp00 $f00
