package dev.team2.daos.tapeworm;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.team2.TapewORM;
import dev.team2.daos.SubscriberDao;
import dev.team2.models.*;
import dev.team2.models.Subscriber;

public class SubscriberDaoImpl implements SubscriberDao{
	private TapewORM orm = new TapewORM();

	public SubscriberDaoImpl() throws SQLException {
	}

	//3 tables: email_table, identity_table, password_table
	//1 shared variable: emailid = id(Subscriber)
	//8 variables in Subscriber
	//LocalDate needs to be changed to Date

	@Override
	public Subscriber addSubscriber(Subscriber subscriber) {
		orm.addDefault("email_table", subscriber.getEmail());
		List<Email> emailList = orm.getColumns("email_table", "email", subscriber.getEmail(), "emailid");
		subscriber.setId(emailList.get(0).getEmailid());
		orm.addDefault("identity_table", subscriber.getId(), subscriber.getLastName(),
				subscriber.getFirstName(), subscriber.getAddress(), subscriber.getPhone());
		orm.addDefault("password_table", subscriber.getId(), subscriber.getPassword());
		return subscriber;
	}

	@Override
	public Subscriber deleteSubscriber(Subscriber subscriber) {
		orm.delete("email_table", "email", subscriber.getEmail());
		orm.delete("identity_table", "emailid", subscriber.getId());
		orm.delete("password_table", "emailid", subscriber.getId());
		return subscriber;
	}

	@Override
	public List<String> getSubscribers() {
		List<Email> email = orm.getAll("email_table");
		List<Identity> identity = orm.getAll("identity_table");
		List<Password> password = orm.getAll("password_table");
		List<Subscriber> subscribers = new ArrayList<Subscriber>();
		
		for (int i = 0; i < email.size(); i++) {
			Subscriber tempSub = new Subscriber();
			tempSub.setId(email.get(i).getEmailid());
			tempSub.setEmail(email.get(i).getEmail());
			tempSub.setFirstName(identity.get(i).getFirstname());
			tempSub.setLastName(identity.get(i).getLastname());
			tempSub.setAddress(identity.get(i).getAddress());
			tempSub.setPhone(identity.get(i).getPhone());
			tempSub.setPassword(password.get(i).getPassword());
			subscribers.add(tempSub);
		}
		
		List<String> subscribeString = new ArrayList<String>();
		for (Subscriber s: subscribers) {
			subscribeString.add(s.getEmail());
		}
		
		return subscribeString;
	}

	@Override
	public Subscriber replacePassword(Subscriber subscriber, String password) {
		orm.update("password_table", "password", password, "emailid", subscriber.getId());
		subscriber.setPassword(password);
		return subscriber;
	}


}
