package dev.team2.services;

import dev.team2.daos.SubscriberDao;
import dev.team2.daos.tapeworm.SubscriberDaoImpl;
import dev.team2.models.Subscriber;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Mailing list subscription service layer.
 */
public class SubscriptionService {

    private SubscriberDao subscriberDao = new SubscriberDaoImpl();

    public SubscriptionService(SubscriberDao subscriberDao) throws SQLException {
        this.subscriberDao = subscriberDao;
    }

    public Subscriber add(Subscriber subscriber) {
        return subscriberDao.addSubscriber(subscriber);
    };

    public Subscriber remove(Subscriber subscriber) {
        return subscriberDao.deleteSubscriber(subscriber);
    }

    public List<String> getAll() {
        return subscriberDao.getSubscribers();
    }

    public Subscriber replacePassword(Subscriber subscriber, String password) {
        return subscriberDao.replacePassword(subscriber, password);
    }

}
